#!/usr/bin/env sh

if grep -Fxq "[backend]" /etc/netdata/netdata.conf
then
  sed  '/\[backend\]/a prefix='$PREFIX_NETDATA /etc/netdata/netdata.conf
else
  # bash -c 'cat << EOF >> /etc/netdata/netdata.conf
  # [backend]
  # prefix='$PREFIX_NETDATA'
  #
  # EOF'
  echo "[backend]" >> /etc/netdata/netdata.conf
  echo "\t prefix="$PREFIX_NETDATA >>/etc/netdata/netdata.conf
fi
