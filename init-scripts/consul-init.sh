#! /bin/bash
sudo bash -c 'cat << EOF >> /etc/environment
export PATH=$PATH:/opt/bin
EOF'
source /etc/environment
sleep 1
sudo mkdir -p /opt
sleep 1
sudo mkdir -p /opt/bin
sleep 1
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-Linux-x86_64 -o /opt/bin/docker-compose
sudo chmod +x /opt/bin/docker-compose
cd /home/core
sudo git clone https://gitlab.com/than-tryf/unicorn-infra.git
sudo chmod -R +x unicorn-infra/init-scripts/*.sh
cd unicorn-infra/compose-containers/consul-compose
docker-compose -f docker-compose-consul-server.yaml up -d
