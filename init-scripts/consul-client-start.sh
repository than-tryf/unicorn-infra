#!/usr/bin/env bash

sudo wget https://releases.hashicorp.com/consul/1.1.0/consul_1.1.0_linux_amd64.zip
sudo unzip consul_1.1.0_linux_amd64.zip
sudo cp consul /opt/bin/conul
# rm consul* -rf
sudo mkdir $HOME/consul
sudo mkdir $HOME/consul/data/

sudo cp consul_server.service /etc/systemd/system/consul_server.service

sudo systemctl daemon-reload
sudo systemctl start consul_server.service
