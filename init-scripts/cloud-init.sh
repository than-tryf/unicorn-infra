#!/usr/bin/env bash

#PROXY ENV. VARS.

# Comment out if no proxy needed
# sudo bash -c 'cat << EOF >> /home/core/profile.sh
#
# export HTTP_PROXY=http://proxy.cs.ucy.ac.cy:8008/
# export HTTPS_PROXY=https://proxy.cs.ucy.ac.cy:8008/
# export http_proxy=http://proxy.cs.ucy.ac.cy:8008/
# export https_proxy=https://proxy.cs.ucy.ac.cy:8008/
# export NO_PROXY=localhost,127.0.0.0/8
#
# EOF'

# testing to append at /etc/environment file

sudo bash -c 'cat << EOF >> /etc/environment

export HTTP_PROXY=http://proxy.cs.ucy.ac.cy:8008/
export HTTPS_PROXY=https://proxy.cs.ucy.ac.cy:8008/
export http_proxy=http://proxy.cs.ucy.ac.cy:8008/
export https_proxy=https://proxy.cs.ucy.ac.cy:8008/
export NO_PROXY=localhost,127.0.0.0/8
export PATH=$PATH:/opt/bin

EOF'

# . /home/core/profile.sh
. /etc/environment


# FIX DOCKER THROUGH PROXY
sudo mkdir /etc/systemd/system/docker.service.d
sudo touch /etc/systemd/system/docker.service.d/http-proxy.conf
sudo bash -c 'cat << EOF > /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://proxy.cs.ucy.ac.cy:8008/"
Environment="HTTPS_PROXY=https://proxy.cs.ucy.ac.cy:8008/"
Environment="NO_PROXY=localhost,127.0.0.0/8"
EOF'
sudo systemctl daemon-reload
sudo systemctl restart docker

# Install compose
sudo mkdir -p /opt/bin/
sudo curl -x http://proxy.cs.ucy.ac.cy:8008/ -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-Linux-x86_64 -o /opt/bin/docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-Linux-x86_64 -o /opt/bin/docker-compose
sudo chmod +x /opt/bin/docker-compose


# Install consul
# sudo curl -x http://proxy.cs.ucy.ac.cy:8008/ -L https://releases.hashicorp.com/consul/1.1.0/consul_1.1.0_linux_amd64.zip -o /opt/bin/consul.zip
# sudo unzip /opt/bin/consul.zip
# sudo chmod +x /opt/bin/consul


#clone repo
cd /home/core
git clone https://gitlab.com/than-tryf/unicorn-infra.git

#
# cd unicorn-infra/consul-compose
# docker-compose up -d
sudo chmod -R +x unicorn-infra/init-scripts/*.sh

# install netdata using proxy
./unicorn-infra/init-scripts/netdata-install-proxy.sh --dont-wait --accept


# change /etc/netdata/netdata.conf to change name
# sed  '/\[option\]/a Hello World' input
