#!/usr/bin/env bash

consul="consul"
version="1.2.0"
arch="linux_amd64"
extension="zip"
baseUrl="https://releases.hashicorp.com"
directory="/opt/bin"
url="${baseUrl}/${consul}/${version}/${consul}_${version}_${arch}.${extension}"

function check_tool() {
  which "${1}"  || command -v "${1}"
}

consultool="$(check_tool consul)"
unziptool="$(check_tool unzip)"


#### CS UCY SPECIFICS #####
# sudo bash -c 'cat << EOF >> /etc/environment
#
# export HTTP_PROXY=http://proxy.cs.ucy.ac.cy:8008/
# export HTTPS_PROXY=https://proxy.cs.ucy.ac.cy:8008/
# export http_proxy=http://proxy.cs.ucy.ac.cy:8008/
# export https_proxy=https://proxy.cs.ucy.ac.cy:8008/
# export NO_PROXY=localhost,127.0.0.0/8
# export PATH=$PATH:/opt/bin
#
# EOF'
# . /etc/environment
##########################


if [ ! -z "${consultool}" -a -x "${consultool}" ]
then
  tput setaf 7; tput bold; echo -n "consul installed"; tput sgr0; tput setaf 2; tput bold; echo -e " \t\t[OK]"; tput sgr0
else
  tput setaf 7; tput bold; echo -n "consul installed"; tput sgr0; tput setaf 1; tput bold; echo -e " \t\t[TO BE INSTALLED]"; tput sgr0
  wget -q "${url}"
  if [ -z "${unziptool}" ]
  then
    sudo apt install -y unzip
  fi
  unzip -qq "${consul}_${version}_${arch}.zip"
  if [ ! -d "${directory}" ]; then
    sudo mkdir /opt/bin/
  fi
  sudo cp consul /opt/bin/consul
  if [ ! -z "${consultool}" -a -x "${consultool}" ]
  then
    tput clear;tput setaf 7; tput bold; echo -n "consul installed"; tput sgr0; tput setaf 2; tput bold; echo -e " \t\t[OK]"; tput sgr0
  fi
  if [ ! -d "$HOME/consul" ]; then
    sudo mkdir $HOME/consul/
    if [ ! -d "${directory}" ]; then
      sudo mkdir $HOME/consul/data/
    fi
  fi
  sudo touch /etc/systemd/system/consul.service
  sudo bash -c 'cat << EOF > /etc/systemd/system/consul.service
[Unit]
Description=CONSUL Server

[Service]
EnvironmentFile=/etc/environment
Type=simple
ExecStart=/bin/sh -c "/opt/bin/consul agent -ui -data-dir $HOME/consul/data -server -bootstrap-expect 1 -client 0.0.0.0 -advertise 10.16.3.157"
Restart=always

[Install]
WantedBy=multi-user.target
EOF'
  sudo systemctl daemon-reload
  sudo systemctl enable consul.service
  sudo systemctl start consul.service
fi

sudo rm consul -rf
sudo rm "${consul}_${version}_${arch}.zip" -rf
