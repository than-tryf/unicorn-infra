
# IPs

Description |IP:PORT
---|---
Prometheus  |http://35.202.233.199:9090/graph
Consul|http://35.192.48.236:8500/ui/
Insights|http://35.206.105.112:7999
Dummy-App1|http://35.225.0.202:8080/
Dummy-App2|http://35.224.93.95:8080/
Netdata_Dummy1|http://35.225.0.202:19999/
Netdata_Dummy2|http://35.224.93.95:19999/  

# How to run

1. Make sure that all .sh files are execs. If not run (cd in the project parent folder) ```sudo chmod +x *.sh```

2. Run the ```init-gcloud.sh``` script. This script will open your webbrowser and you should login to your google cloud account. After that all scripts that are based on gcloud will be authorized to run with out further authentication

3. Scripts that perform actions
  1. ```register-dummy-1.sh```  This script will register the Dummy1 App to Consul.
  * ```deregister-dummy-1.sh``` This scrit will deregister Dummy 1 App from consul
  * ```register-dummy-2.sh``` This script will register the Dummy2 App to Consul.
  * ```deregister-dummy-2.sh``` This script will deregister Dummy2 App from Consul.

* If the dummy apps for some reason become unresponsive please run the following scripts:
  1. For dummy1: ``` ./reset-dummy-1.sh ```

  * For dummy2: ``` ./reset-dummy-2.sh ```


## Scaling Rules
* Scale Out : When cpu_util  >=  70%
* Scale In: When cpu_idle >= 85%




### Useful Resources
* https://imaginea.gitbooks.io/consul-devops-handbook/content/deploying_consul_in_docker_containers.html
* https://www.consul.io/docs/guides/external.html
* https://www.consul.io/api/catalog.html
* https://sreeninet.wordpress.com/2016/04/17/service-discovery-with-consul/
* https://www.robustperception.io/finding-consul-services-to-monitor-with-prometheus/



### Other -- For Internal Usage -- Don't Mind the text below!
curl -X PUT --data @dummy.json http://10.16.3.113:8500/v1/catalog/register
curl -X PUT --data @dummy-config.json http://35.192.48.236:8500/v1/catalog/register
....
