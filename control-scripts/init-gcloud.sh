#!/usr/bin/env bash

function check_tool() {
   which "${1}"  || command -v "${1}"
}

tput setaf 6; tput bold; cat misc/banner ; tput sgr0;



gcloud="$(check_tool gcloud)"

echo -e "\n\n"

if [ ! -z "${gcloud}" -a -x "${gcloud}" ]
then
  tput setaf 7; tput bold; echo -n "gcloud SDK installed"; tput sgr0; tput setaf 2; tput bold; echo -e " \t\t[OK]"; tput sgr0
else
  tput setaf 7; tput bold; echo -e "Installing gcloud SDK...."; tput sgr0; tput setaf 1;
  tput setaf 1; echo -e "\tInstalling..."; tput sgr0;
  export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
  echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
  curl -Ss https://packages.cloud.google.com/apt/doc/apt-key.gpg  | sudo apt-key add > /dev/null
  sudo apt-get -qq update && sudo apt-get -y -qq install google-cloud-sdk > /dev/null
  gcloud="$(check_tool gcloud)"

  if [ ! -z "${gcloud}" -a -x "${gcloud}" ]
  then
    tput setaf 7; tput bold; echo -n "gcloud SDK installed"; tput sgr0; tput setaf 2; tput bold; echo -e " \t\t[OK]"; tput sgr0
  else
    exit 1
  fi
fi

# gcloud init --console-only
gcloud auth login --brief

# gcloud compute instances create consul --image-project coreos-cloud --image-family coreos-stable --zone us-central1-a --machine-type n1-standard-1 --metadata-from-file user-data=config.ign

# setup consul vm
# gcloud compute instances create consul --zone=us-central1-a --machine-type=n1-standard-1 --subnet=unicorn-subnet --can-ip-forward --image=coreos-stable-1745-5-0-v20180531 --image-project=coreos-cloud --metadata-from-file startup-script=init-scripts/consul-init.sh
# setup prometheus vm
#gcloud compute instances create prometheus --zone=us-central1-a --machine-type=n1-standard-1 --subnet=unicorn-subnet --can-ip-forward --image=coreos-stable-1745-5-0-v20180531 --image-project=coreos-cloud --metadata-from-file startup-script=init-scripts/consul-init.sh

# setup insights vm

# setup dummy app vm
# gcloud compute instances create insights --zone=us-central1-a --machine-type=n1-standard-1 --subnet=unicorn-subnet --can-ip-forward --image=coreos-stable-1745-5-0-v20180531 --image-project=coreos-cloud --metadata-from-file startup-script=init-scripts/consul-init.sh
# gcloud compute ssh thanasis@dummy --command="cd /home/core/unicorn-infra/init-scripts && curl -X PUT --data @dummy.json http://35.192.48.236:8500/v1/catalog/register"
